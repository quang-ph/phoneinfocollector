# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import urljoin, urlparse
import resource
import csv
from scrapy_splash import SplashRequest

resource.setrlimit(resource.RLIMIT_NOFILE, (65536, 65536))


class CarSpider(scrapy.Spider):
    name = 'all_phone'
    # download_delay = 2

    with open('phonecollector/spiders/hc.csv', 'rt', encoding="utf8") as f:
        reader = csv.DictReader(f)
        data = {}
        for row in reader:
            for header, value in row.items():
                data.setdefault(header, list()).append(value)
        subject = data['subject']
        selector = data['selector']
        default_value = data['default']

    start_urls = [selector[0]]
    if not selector[1]:
        for i in range(1, int(float(default_value[1]))):
            start_urls.append("https://mediamart.vn/smartphones/?trang=" + str(i+1))
    root_url = '{}://{}'.format(urlparse(start_urls[0]).scheme, urlparse(start_urls[0]).netloc)

    custom_settings = {

        "SPLASH_URL": 'http://localhost:8050',
        "DOWNLOADER_MIDDLEWARES": {
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
            'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
            'scrapy_splash.SplashCookiesMiddleware': 723,
            'scrapy_splash.SplashMiddleware': 725,
            'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
        },
        "USER_AGENT_CHOICES": [
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:23.0) Gecko/20100101 Firefox/23.0',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36',
            'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36',
            'Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20140205 Firefox/24.0 Iceweasel/24.3.0',
            'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0',
            'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
        ],
        "DUPEFILTER_CLASS": 'scrapy_splash.SplashAwareDupeFilter',
        "HTTPCACHE_STORAGE": 'scrapy_splash.SplashAwareFSCacheStorage',
        "SPIDER_MIDDLEWARES": {
            'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
        },
        'RETRY_TIMES': 10,
        'RETRY_HTTP_CODES': [500, 503, 504, 400, 403, 404, 408],
    }

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        car_links = response.xpath(self.selector[2]).extract()
        for link in car_links:
            if link.startswith('/') or link.startswith(self.root_url):
                link = urljoin(self.root_url, link)
                yield scrapy.Request(url=link, callback=self.parse_item)
        next_page = response.xpath(self.selector[1]).extract()
        for page in next_page:
            if page.startswith('/') or page.startswith(self.root_url):
                page = urljoin(self.root_url, page)
                yield scrapy.Request(url=page, callback=self.parse)

    def parse_item(self, response):
        phone = {}
        brand_selector = self.check_input_xpath(response, 3)
        line_selector = self.check_input_xpath(response, 4)
        battery_selector = self.check_input_xpath(response, 5)
        color_selector = self.check_input_xpath(response, 6)
        price_selector = self.check_input_xpath(response, 7)
        cpu_selector = self.check_input_xpath(response, 8)
        os_selector = self.check_input_xpath(response, 9)
        screen_selector = self.check_input_xpath(response, 10)
        camera_selector = self.check_input_xpath(response, 11)
        storage_selector = self.check_input_xpath(response, 12)

        phone['brand'] = self.get_content(brand_selector)[0].strip()
        phone['line'] = self.get_content(line_selector)[0].strip()
        phone['battery'] = self.get_content(battery_selector)[0].strip()
        phone['color'] = self.get_content(color_selector)
        phone['price'] = self.price_format(self.get_content(price_selector)[0])
        phone['chipset'] = self.get_content(cpu_selector)[0].strip()
        phone['os'] = self.get_content(os_selector)[0].strip()
        phone['screen'] = self.get_content(screen_selector)[0].strip()
        phone['camera'] = self.get_content(camera_selector)[0].strip()
        phone['storage'] = self.get_content(storage_selector)[0].strip()
        phone = self.format(phone)
        return phone

    def get_content(self, selector):
        if selector:
            value = selector.extract()
            if len(value) > 0:
                return value
        else:
            return [""]

    def check_input_xpath(self, response, index):
        if len(self.selector[index]) > 0:
            return response.xpath(self.selector[index])

    def price_format(self, price_txt):
        if "000" in price_txt:
            price_txt = price_txt.replace(".", "").replace("₫", "").replace("đ", "").strip()
            price = int(float(price_txt))
        else:
            price = ""
        return price

    def format(self, phone):
        phone['line'] = phone['line'].replace("Điện thoại", "").replace(phone['brand'], "").replace("di động", "").strip()
        phone['brand'] = phone['brand'].replace("Smartphones ", "")
        phone['screen'] = phone['screen'].replace("\"", "inch").replace("inches", "inch")
        return phone